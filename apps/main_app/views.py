from django.shortcuts import render, HttpResponse, redirect
import re, bcrypt
from django.contrib import messages
from .models import *

EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
# the index function is called when root is visited

def index(request):
    return render(request,'main_app/index.html')

def register(request):
    request.session['error'] = False
    try:
        user = User.objects.get(email=request.POST['email'])
    except:
        #is the email empty or not enough chars?
        if len(request.POST['email']) < 1:
            messages.error(request, "Email cannot be blank!")
            request.session['error'] = True
        elif not EMAIL_REGEX.match(request.POST['email']):
            messages.error(request, "Invalid Email Address!")
            request.session['error'] = True

        #is the first name empty or not enough chars?
        if len(request.POST['first_name']) < 1:
            messages.error(request, "First name cannot be blank!")
            request.session['error'] = True
        elif len(request.POST['first_name']) <= 3:
            messages.error(request, "First name must be 3+ characters")
            request.session['error'] = True

        #is the last name empty or not enough chars?
        if len(request.POST['last_name']) < 1:
            messages.error(request, "Last name cannot be blank!")
            request.session['error'] = True
        elif len(request.POST['last_name']) <= 2:
            messages.error(request, "Last name must be minimum 2 characters")
            request.session['error'] = True

        #is the password empty or not enough chars?
        if len(request.POST['password']) < 1:
            messages.error(request, "Password cannot be blank!")
            request.session['error'] = True
        elif len(request.POST['password']) <= 5:
            messages.error(request, "Password must be minimum 5 characters")
            request.session['error'] = True

        #do the passwords match?
        if not (request.POST['password_confirmation'] == request.POST['password']):
            messages.error(request, "Passwords must match!")

    #does this user exist? if so log the message and set session error=True
    if 'user' in locals():
        messages.error(request, "User with that Email address already exists")
        request.session['error'] = True

    #if any errors, take them back and send messages
    if request.session['error'] == True: 
        return redirect('/')
    #otherwise lets hash the password, save their info and log them in
    else:
        password_hash = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt())
        users = User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'], password=password_hash)
        request.session['userid'] = users.id
        request.session['logged'] = True
        return redirect('/dashboard')



def login(request):
    request.session['error'] = False #sets our session error default
    #lets do some form validations when logging in
    #is the email empty or not enough chars?
    if len(request.POST['login_email']) < 1:
        messages.error(request, "Email cannot be blank!")
        request.session['error'] = True
    elif not EMAIL_REGEX.match(request.POST['login_email']):
        messages.error(request, "Invalid Email Address!")
        request.session['error'] = True
    #is the password empty or not enough chars?
    if len(request.POST['login_password']) < 1:
        messages.error(request, "Password cannot be blank!")
        request.session['error'] = True
    elif len(request.POST['login_password']) <= 5:
        messages.error(request, "Password must be minimum 5+ characters")
        request.session['error'] = True
    #does this email/user exist in our database?
    try:
        user = User.objects.get(email=request.POST['login_email'])
    except User.DoesNotExist:
        messages.error(request, "No user found with that email address")
        request.session['error'] = True
    except:
        messages.error(request, "Yikes! Something else went wrong")
    #check if password is valid via bcrypt
    password_valid = bcrypt.checkpw(request.POST['login_password'].encode(), user.password.encode())
    if password_valid == True:
            request.session['logged'] = True
            request.session['userid'] = user.id
    else:
        messages.error(request, "Password/Email Address must match")
        request.session['error'] = True
    if request.session['error'] == True:
        return redirect('/')
    # seems everything checks out, lets forward them to our dashboard
    return redirect('/dashboard')


#create our dashboard object
def dashboard(request):
    # if user is not logged in, redirect them back
    if 'userid' not in request.session:
        messages.error(request, "You are not logged in!")
        return redirect('/')
    else:
    #user is now logged in
        user = User.objects.get(id=request.session['userid'])
        my_jobs = Job.objects.filter(creater_id=user.id)
        available_jobs = Job.objects.exclude(creater_id=user.id)
        #setup our database dict
        context = {
            'user': user,
            'myjobs': my_jobs,
            'jobs': available_jobs
        }
    return render(request,'main_app/dashboard.html', context)


# create our job views below

def addjob(request):
    return render(request,'main_app/addJob.html')


def addtolist(request,job_id):
    job = Job.objects.get(id=job_id)
    job.creater_id = request.session['userid']
    job.save()
    return redirect('/dashboard')


def processjob(request):
    request.session['error'] = False
    if len(request.POST['job_name']) < 3:
        messages.error(request, "Title must be more than 3 characters!")
        request.session['error'] = True
    if len(request.POST['description']) < 10:
        messages.error(request, "description must me more than 10 characters")
        request.session['error'] = True
    if len(request.POST['location']) < 1:
        messages.error(request, "location cannot be blank")
        request.session['error'] = True
    if request.session['error'] == True:
        return redirect('/addJob')
    else:
        user = User.objects.get(id=request.session['userid'])
        Job.objects.create(job_name=request.POST['job_name'],location=request.POST['location'],description=request.POST['description'],creater_id=0, user = user)
        return redirect('/dashboard')


def removejob(request, job_id):
    job = Job.objects.get(id=job_id)
    job.delete()
    return redirect('/dashboard')



def edit(request, job_id):
    job = Job.objects.get(id=job_id)
    print(Job.objects.all().values())
    context = {
        'job': job
    }
    return render(request,'main_app/edit.html', context)



def processedit(request, job_id):
    request.session['error'] = False
    if len(request.POST['job_name']) < 3:
        messages.error(request, "Title must be more than 3 characters!")
        request.session['error'] = True
    if len(request.POST['description']) < 10:
        messages.error(request, "description must me more than 10 characters")
        request.session['error'] = True
    if len(request.POST['location']) < 1:
        messages.error(request, "location cannot be blank")
        request.session['error'] = True

    if request.session['error'] == True:
        return redirect('/edit/' +str(job_id))
    else:
        job = Job.objects.get(id=job_id)
        job.job_name = request.POST['job_name']
        job.description = request.POST['description']
        job.location = request.POST['location']
        job.save()
    return redirect('/dashboard')



def viewjob(request, job_id):
    job = Job.objects.get(id=job_id)
    user = User.objects.get(id = job.user_id)
    print(user)
    context = {
        'job': job,
        'user': user
    } 
    return render(request,'main_app/view.html', context)



def logout(request):
    request.session.clear()
    return redirect('/')


