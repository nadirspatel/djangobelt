from django.conf.urls import url
from . import views           # This line is new!
urlpatterns = [
    url(r'^$', views.index),   # This line has changed! Notice that urlpatterns is a list, the comma is in
    url(r'^logout', views.logout),
    url(r'^dashboard', views.dashboard),
    url(r'^register', views.register),
    url(r'^login', views.login),
    url(r'^addJob', views.addjob),
    url(r'^processjob', views.processjob), 
    url(r'^addtomyJob/(?P<job_id>\d+)', views.addtolist),
    url(r'^deletejob/(?P<job_id>\d+)', views.removejob),
    url(r'^view/(?P<job_id>\d+)/', views.viewjob),
    url(r'^edit/(?P<job_id>\d+)', views.edit),
    url(r'^processedit/(?P<job_id>\d+)', views.processedit),

]   